package com.example.stephaniecorona.android82;

import java.util.Random;

/**Code for Random movement implementation
 * Created by Stephanie Corona on 5/4/2016.
 */
public class RandomMove {
    public static int type(int player, int PieceType, int startLocation){
        int endLocation =0;

        switch (PieceType){
            case 0:
                if (player ==0){
                    endLocation = startLocation -8;
                }
                else{
                    endLocation = startLocation + 8;
                }
                break;
            case 1:
                Random rand = new Random();
                int  n = rand.nextInt(3);
                int tiles = rand.nextInt(7)+1;
                boolean found = false;
                while (!found){
                    if (n == 0 && !(startLocation-tiles <0 || startLocation-tiles > 63 )){
                        endLocation = startLocation - tiles;
                        found = true;
                    }
                    else if (n==1 && !(startLocation+tiles <0 || startLocation+tiles > 63 )){
                        endLocation = startLocation+tiles;
                        found = true;
                    }
                    else if (n==2 && !(startLocation+(tiles*2) <0 || startLocation+(tiles*2) > 63 )){
                        endLocation = startLocation +(tiles*2);
                        found = true;
                    }
                    else if(n==3 && !(startLocation-(tiles*2) <0 || startLocation-(tiles*2) > 63 )){
                        endLocation = startLocation - (tiles*2);
                        found = true;
                    }
                    n = rand.nextInt(3);
                    tiles = rand.nextInt(7)+1;
                }

                break;
            case 2:
                Random rand2 = new Random();
                int  n2 = rand2.nextInt(7);
                boolean found2= false;
                while (!found2){
                    if (n2 == 0 && !(startLocation+17 < 0 || startLocation+17>63)){
                        endLocation = startLocation + 17;
                        found2 = true;
                    }
                    else if (n2 == 1 && !(startLocation+15 < 0 || startLocation+15>63)){
                        endLocation = startLocation + 15;
                        found2 = true;
                    }
                    else if (n2 == 2 && !(startLocation-15 < 0 || startLocation-15>63)){
                        endLocation = startLocation - 15;
                        found2 = true;
                    }
                    else if (n2 == 3 && !(startLocation-17 < 0 || startLocation-17>63)){
                        endLocation = startLocation - 17;
                        found2 = true;
                    }
                    else if (n2 == 4 && !(startLocation+10 < 0 || startLocation+10>63)){
                        endLocation = startLocation + 10;
                        found2 = true;
                    }
                    else if (n2 == 5 && !(startLocation-6 < 0 || startLocation-6>63)){
                        endLocation = startLocation -6;
                        found2 = true;
                    }
                    else if (n2 == 6 && !(startLocation-10 < 0 || startLocation-10>63)){
                        endLocation = startLocation - 10;
                        found2 = true;
                    }
                    else if (n2 == 7 && !(startLocation+6 < 0 || startLocation+6>63)){
                        endLocation = startLocation + 6;
                        found2 = true;
                    }
                    n2 = rand2.nextInt(7);
                }

                break;
            case 3:
                Random rand3 = new Random();
                int  n3 = rand3.nextInt(3);
                int tiles3 = rand3.nextInt(7)+1;
                boolean found3= false;
                while (!found3){
                    if(n3==0 && !(startLocation + tiles3*8+tiles3 <0 || startLocation + tiles3*8+tiles3 >63 )){
                        endLocation = startLocation + tiles3*8+tiles3;
                        found3 = true;
                    }
                    else if(n3==1 && !(startLocation + tiles3*-8+tiles3<0 || startLocation + tiles3*-8+tiles3 > 63)){
                        endLocation = startLocation + tiles3*-8+tiles3;
                        found3 = true;
                    }
                    else if(n3 == 2 && !(startLocation + tiles3*8-tiles3<0) || startLocation +tiles3*8-tiles3>63){
                        endLocation = startLocation + tiles3*8 - tiles3;
                        found3 = true;
                    }
                    else if(n3 == 3 && !(startLocation + tiles3*-8 -tiles3<0 || startLocation +tiles3*-8-tiles3 >63)){
                        endLocation = startLocation + tiles3*-8-tiles3;
                        found3 = true;
                    }
                }
                break;
            case 4:
                if (player ==0){

                }
                else{

                }
                break;
            case 5:
                if (player ==0){

                }
                else{

                }
                break;
        }

        return endLocation;
    }
}
