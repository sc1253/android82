package com.example.stephaniecorona.android82;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Recorded_Games extends AppCompatActivity {
    public static String gameName[];
    private final static String STORETEXT="Games";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorded__games);

        readFileInEditor();
        populateListView();
        registerClickCallback();

        TextView tv = (TextView) findViewById(R.id.savedGameMoves);
        tv.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option1:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.option2:
                Intent intent = new Intent(this, Recorded_Games.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.option3:
                Intent intent2 = new Intent(this, Old_Games.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void populateListView(){

        String array[]= gameName;

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.games, array);
        ListView list = (ListView) findViewById(R.id.recordedGames);
        list.setAdapter(adapter);
    }

    public void readFileInEditor() {
        try {
            InputStream in = openFileInput(STORETEXT);
            if (in != null) {
                InputStreamReader tmp=new InputStreamReader(in);
                BufferedReader reader=new BufferedReader(tmp);
                String str;
                StringBuilder buf=new StringBuilder();
                while ((str = reader.readLine()) != null) {
                    buf.append(str+"\n");
                }
                in.close();
                String games = buf.toString();
                gameName = games.split("\n");
            }
        }

        catch (java.io.FileNotFoundException e) {
        }

        catch (Throwable t) {
            Toast.makeText(this, "Exception: "+t.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void registerClickCallback() {
        ListView list = (ListView) findViewById(R.id.recordedGames);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> paret, View viewClicked, int position, long id) {
                TextView textView = (TextView) viewClicked;
                String file = textView.getText().toString();
                String moves = readFileInEditor(file);
                TextView saved = (TextView) findViewById(R.id.savedGameMoves);
                saved.setText("");
                saved.append(file +"'s Game Moves\n");
                saved.append(moves);

            }
        });
    }

    public String readFileInEditor(String fileName) {
        String games="";
        try {
            InputStream in = openFileInput(fileName);
            if (in != null) {
                InputStreamReader tmp=new InputStreamReader(in);
                BufferedReader reader=new BufferedReader(tmp);
                String str;
                StringBuilder buf=new StringBuilder();
                while ((str = reader.readLine()) != null) {
                    buf.append(str+"\n");
                }
                in.close();
                games = buf.toString();
            }
        }

        catch (java.io.FileNotFoundException e) {
        }

        catch (Throwable t) {
            Toast.makeText(this, "Exception: "+t.toString(), Toast.LENGTH_LONG).show();
        }
        return games;
    }

}
