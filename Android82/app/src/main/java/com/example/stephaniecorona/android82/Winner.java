package com.example.stephaniecorona.android82;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Winner extends AppCompatActivity {
    private String m_Text = "";
    private final static String STORETEXT="Games";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        TextView player = (TextView) findViewById(R.id.winner);
        ImageView winImage = (ImageView) findViewById(R.id.winnerImage);

        int move[] = MainActivity.moves;

        boolean draw = MainActivity.draw;
        if (draw){
            if (MainActivity.playersTurn == 0){
                player.setText("Black Player Wins");
                winImage.setImageResource(R.drawable.b_king);
            }
            else{
                player.setText("White Player Wins");
                winImage.setImageResource(R.drawable.w_king);
            }
        }
        else{
            if (MainActivity.winner == 0){
                player.setText("White Player Wins");
                winImage.setImageResource(R.drawable.w_king);
            }
            else{
                player.setText("Black Player Wins");
                winImage.setImageResource(R.drawable.b_king);
            }

        }

        Button newGame = (Button) findViewById(R.id.new_game);
        newGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        Button saveGame = (Button) findViewById(R.id.save);
        saveGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Winner.this);
                builder.setTitle("New Game");

                final EditText input = new EditText(Winner.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_Text = input.getText().toString();
                        String str = ReadFile();
                        openFile(m_Text, str);
                        String moveText=convertOldMoves();
                        createNewFiles(m_Text,moveText);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.option1:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.option2:
                Intent intent = new Intent(this, Recorded_Games.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.option3:
                Intent intent2 = new Intent(this, Old_Games.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    public String ReadFile(){
        String games="";
        try {
            InputStream in = openFileInput(STORETEXT);
            if (in != null) {
                InputStreamReader tmp=new InputStreamReader(in);
                BufferedReader reader=new BufferedReader(tmp);
                String str;
                StringBuilder buf=new StringBuilder();
                while ((str = reader.readLine()) != null) {
                    buf.append(str+"\n");
                }
                in.close();
                games = buf.toString();
            }
        }

        catch (java.io.FileNotFoundException e) {
        }

        catch (Throwable t) {
            Toast.makeText(this, "Exception: "+t.toString(), Toast.LENGTH_LONG).show();
        }
        return games;
    }

    public void openFile(String m_Text, String str){
        try {
            OutputStreamWriter out= new OutputStreamWriter(openFileOutput(STORETEXT, 0));
            out.write(str);
            out.write(m_Text +"\n");
            out.close();
        }
        catch (Throwable t) {
        }

    }

    public void createNewFiles(String filename, String fileText){
        try {
            OutputStreamWriter out= new OutputStreamWriter(openFileOutput(filename, 0));
            out.write(fileText);
            out.close();
        }
        catch (Throwable t) {
        }
    }
    public String convertOldMoves(){
        int savedMoves[] = MainActivity.moves;
        String moveText ="";
        for (int i=0; savedMoves[i] != -1; i++){
            moveText += convertMove(savedMoves[i]);
            if(i!=0 && i%2==1){
                moveText += "\n";
            }
        }
        return moveText;
    }
    public String convertMove(int move) {
        int letter = move % 8;
        int num = move / 8;
        String moveLetter = "";

        switch (letter) {
            case 7:
                moveLetter += "h";
                break;
            case 6:
                moveLetter += "g";
                break;
            case 5:
                moveLetter += "f";
                break;
            case 4:
                moveLetter += "e";
                break;
            case 3:
                moveLetter += "d";
                break;
            case 2:
                moveLetter += "c";
                break;
            case 1:
                moveLetter += "b";
                break;
            case 0:
                moveLetter += "a";
                break;
        }

        switch (num) {
            case 7:
                moveLetter += "1";
                break;
            case 6:
                moveLetter += "2";
                break;
            case 5:
                moveLetter += "3";
                break;
            case 4:
                moveLetter += "4";
                break;
            case 3:
                moveLetter += "5";
                break;
            case 2:
                moveLetter += "6";
                break;
            case 1:
                moveLetter += "7";
                break;
            case 0:
                moveLetter += "8";
                break;
        }
        moveLetter += " ";
        return moveLetter;
    }
}

