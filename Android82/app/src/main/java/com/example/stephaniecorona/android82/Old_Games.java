package com.example.stephaniecorona.android82;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Old_Games extends AppCompatActivity {
    public static String gameName[];
    private final static String STORETEXT="Games";
    public static String GameFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old__games);

        readFileInEditor();
        populateListView();
        registerClickCallback();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option1:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.option2:
                Intent intent = new Intent(this, Recorded_Games.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.option3:
                Intent intent2 = new Intent(this, Old_Games.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Populates ListView
     * @author Stephanie Corona
     */
    private void populateListView(){

        String array[]= gameName;

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.games, array);
        ListView list = (ListView) findViewById(R.id.games);
        list.setAdapter(adapter);
    }

    /**
     * Checks file to gather list to populate ListView with
     * @author Stephanie Corona
     */

    public void readFileInEditor() {
        TextView text = (TextView) findViewById(R.id.textView2);

        try {
            InputStream in = openFileInput(STORETEXT);
            if (in != null) {
                InputStreamReader tmp=new InputStreamReader(in);
                BufferedReader reader=new BufferedReader(tmp);
                String str;
                StringBuilder buf=new StringBuilder();
                while ((str = reader.readLine()) != null) {
                    buf.append(str+"\n");
                }
                in.close();
                String games = buf.toString();
                gameName = games.split("\n");
            }
        }

        catch (java.io.FileNotFoundException e) {
        }

        catch (Throwable t) {
            Toast.makeText(this, "Exception: "+t.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void registerClickCallback() {
        ListView list = (ListView) findViewById(R.id.games);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> paret, View viewClicked, int position, long id) {
                TextView textView2 = (TextView) viewClicked;
                String file = textView2.getText().toString();
                GameFile = file;

                Intent intent = new Intent(Old_Games.this, ReplayGame.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
}
