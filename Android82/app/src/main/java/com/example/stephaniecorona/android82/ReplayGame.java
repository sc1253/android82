package com.example.stephaniecorona.android82;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ReplayGame extends AppCompatActivity {
    private ImageView grid[];
    public int i=0;
    int GameMoves[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay_game);


        //ImageView setup
        grid = new ImageView[64];
        grid[0] = (ImageView) findViewById(R.id.a8);
        grid[1] = (ImageView) findViewById(R.id.b8);
        grid[2] = (ImageView) findViewById(R.id.c8);
        grid[3] = (ImageView) findViewById(R.id.d8);
        grid[4] = (ImageView) findViewById(R.id.e8);
        grid[5] = (ImageView) findViewById(R.id.f8);
        grid[6] = (ImageView) findViewById(R.id.g8);
        grid[7] = (ImageView) findViewById(R.id.h8);
        //------------------------------------------------
        grid[8] = (ImageView) findViewById(R.id.a7);
        grid[9] = (ImageView) findViewById(R.id.b7);
        grid[10] = (ImageView) findViewById(R.id.c7);
        grid[11] = (ImageView) findViewById(R.id.d7);
        grid[12] = (ImageView) findViewById(R.id.e7);
        grid[13] = (ImageView) findViewById(R.id.f7);
        grid[14] = (ImageView) findViewById(R.id.g7);
        grid[15] = (ImageView) findViewById(R.id.h7);
        //------------------------------------------------
        grid[16] = (ImageView) findViewById(R.id.a6);
        grid[17] = (ImageView) findViewById(R.id.b6);
        grid[18] = (ImageView) findViewById(R.id.c6);
        grid[19] = (ImageView) findViewById(R.id.d6);
        grid[20] = (ImageView) findViewById(R.id.e6);
        grid[21] = (ImageView) findViewById(R.id.f6);
        grid[22] = (ImageView) findViewById(R.id.g6);
        grid[23] = (ImageView) findViewById(R.id.h6);
        //------------------------------------------------
        grid[24] = (ImageView) findViewById(R.id.a5);
        grid[25] = (ImageView) findViewById(R.id.b5);
        grid[26] = (ImageView) findViewById(R.id.c5);
        grid[27] = (ImageView) findViewById(R.id.d5);
        grid[28] = (ImageView) findViewById(R.id.e5);
        grid[29] = (ImageView) findViewById(R.id.f5);
        grid[30] = (ImageView) findViewById(R.id.g5);
        grid[31] = (ImageView) findViewById(R.id.h5);
        //------------------------------------------------
        grid[32] = (ImageView) findViewById(R.id.a4);
        grid[33] = (ImageView) findViewById(R.id.b4);
        grid[34] = (ImageView) findViewById(R.id.c4);
        grid[35] = (ImageView) findViewById(R.id.d4);
        grid[36] = (ImageView) findViewById(R.id.e4);
        grid[37] = (ImageView) findViewById(R.id.f4);
        grid[38] = (ImageView) findViewById(R.id.g4);
        grid[39] = (ImageView) findViewById(R.id.h4);
        //------------------------------------------------
        grid[40] = (ImageView) findViewById(R.id.a3);
        grid[41] = (ImageView) findViewById(R.id.b3);
        grid[42] = (ImageView) findViewById(R.id.c3);
        grid[43] = (ImageView) findViewById(R.id.d3);
        grid[44] = (ImageView) findViewById(R.id.e3);
        grid[45] = (ImageView) findViewById(R.id.f3);
        grid[46] = (ImageView) findViewById(R.id.g3);
        grid[47] = (ImageView) findViewById(R.id.h3);
        //------------------------------------------------
        grid[48] = (ImageView) findViewById(R.id.a2);
        grid[49] = (ImageView) findViewById(R.id.b2);
        grid[50] = (ImageView) findViewById(R.id.c2);
        grid[51] = (ImageView) findViewById(R.id.d2);
        grid[52] = (ImageView) findViewById(R.id.e2);
        grid[53] = (ImageView) findViewById(R.id.f2);
        grid[54] = (ImageView) findViewById(R.id.g2);
        grid[55] = (ImageView) findViewById(R.id.h2);
        //------------------------------------------------
        grid[56] = (ImageView) findViewById(R.id.a1);
        grid[57] = (ImageView) findViewById(R.id.b1);
        grid[58] = (ImageView) findViewById(R.id.c1);
        grid[59] = (ImageView) findViewById(R.id.d1);
        grid[60] = (ImageView) findViewById(R.id.e1);
        grid[61] = (ImageView) findViewById(R.id.f1);
        grid[62] = (ImageView) findViewById(R.id.g1);
        grid[63] = (ImageView) findViewById(R.id.h1);

        //Assign black piece values to grid
        grid[0].setTag("b_rook");
        grid[1].setTag("b_knight");
        grid[2].setTag("b_bishop");
        grid[3].setTag("b_queen");
        grid[4].setTag("b_king");
        grid[5].setTag("b_bishop");
        grid[6].setTag("b_knight");
        grid[7].setTag("b_rook");

        //Assign black pawn tag to grid values
        for(int i=8; i<16; i++) {
            grid[i].setTag("b_pawn");
        }
        //Assign blank backgrounds to all other grid values
        for(int i=16; i<48; i++){
            grid[i].setTag("blank");
        }
        //Assign white pawn to all other grid values
        for(int i=48; i<56; i++) {
            grid[i].setTag("w_pawn");
        }
        //Assign white pawn tag to grid values
        grid[56].setTag("w_rook");
        grid[57].setTag("w_knight");
        grid[58].setTag("w_bishop");
        grid[59].setTag("w_queen");
        grid[60].setTag("w_king");
        grid[61].setTag("w_bishop");
        grid[62].setTag("w_knight");
        grid[63].setTag("w_rook");

        String gameInfo = readFile(Old_Games.GameFile);
        GameMoves = new int[Moves(gameInfo).length];
        GameMoves = Moves(gameInfo);

        startGame();
    }

    public void startGame(){
        i=0;
        Button newGame = (Button) findViewById(R.id.newGame);
        newGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        Button next = (Button) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (i<GameMoves.length) {
                    String oldColor = (String) grid[GameMoves[i]].getTag();
                    grid[GameMoves[i]].setImageResource(R.drawable.blank);
                    grid[GameMoves[i]].setTag("blank");

                    if (oldColor.equals("blank")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.blank);
                        grid[GameMoves[i + 1]].setTag("blank");
                    } else if (oldColor.equals("b_pawn")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.b_pawn);
                        grid[GameMoves[i + 1]].setTag("b_pawn");
                    } else if (oldColor.equals("b_rook")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.b_rook);
                        grid[GameMoves[i + 1]].setTag("b_rook");
                    } else if (oldColor.equals("b_knight")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.b_knight);
                        grid[GameMoves[i + 1]].setTag("b_knight");
                    } else if (oldColor.equals("b_queen")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.b_queen);
                        grid[GameMoves[i + 1]].setTag("b_queen");
                    } else if (oldColor.equals("b_king")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.b_king);
                        grid[GameMoves[i + 1]].setTag("b_king");
                    } else if (oldColor.equals("b_bishop")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.b_bishop);
                        grid[GameMoves[i + 1]].setTag("b_bishop");
                    } else if (oldColor.equals("w_pawn")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.w_pawn);
                        grid[GameMoves[i + 1]].setTag("w_pawn");
                    } else if (oldColor.equals("w_rook")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.w_rook);
                        grid[GameMoves[i + 1]].setTag("w_rook");
                    } else if (oldColor.equals("w_knight")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.w_knight);
                        grid[GameMoves[i + 1]].setTag("w_knight");
                    } else if (oldColor.equals("w_queen")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.w_queen);
                        grid[GameMoves[i + 1]].setTag("w_queen");
                    } else if (oldColor.equals("w_king")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.w_king);
                        grid[GameMoves[i + 1]].setTag("w_king");
                    } else if (oldColor.equals("w_bishop")) {
                        grid[GameMoves[i + 1]].setImageResource(R.drawable.w_bishop);
                        grid[GameMoves[i + 1]].setTag("w_bishop");
                    }
                }
                i+=2;
            }
        });

    }

    public int[] Moves(String gameInfo){
        String gameMoves[] = gameInfo.split("\n");
        int length = gameMoves.length * 2;
        int moves[] = new int[length];
        int count=0;

        for(int j=0; j<gameMoves.length; j++){
            String str[] = gameMoves[j].split(" ");
            moves[count] = convertValues(str[0]);
            moves[count+1] = convertValues(str[1]);
            count +=2;
        }
        return moves;
    }

    public int convertValues(String move){
        char col = move.charAt(0);
        char row = move.charAt(1);
        int colNum=0;
        int rowNum=0; int location=0;

        switch (col){
            case 'a':
                colNum=0;
                break;
            case 'b':
                colNum=1;
                break;
            case 'c':
                colNum=2;
                break;
            case 'd':
                colNum=3;
                break;
            case 'e':
                colNum=4;
                break;
            case 'f':
                colNum=5;
                break;
            case 'g':
                colNum=6;
                break;
            case 'h':
                colNum=7;
                break;
            default:
                colNum = -1;
        }

        switch (row){
            case '1':
                rowNum=7;
                break;
            case '2':
                rowNum=6;
                break;
            case '3':
                rowNum=5;
                break;
            case '4':
                rowNum=4;
                break;
            case '5':
                rowNum=3;
                break;
            case '6':
                rowNum=2;
                break;
            case '7':
                rowNum=1;
                break;
            case '8':
                rowNum=0;
                break;
            default :
                rowNum = -1;
        }
        location = rowNum*8 + colNum;
        return location;
    }

    public String readFile(String file){
        String games="";
        try {
            InputStream in = openFileInput(file);
            if (in != null) {
                InputStreamReader tmp=new InputStreamReader(in);
                BufferedReader reader=new BufferedReader(tmp);
                String str;
                StringBuilder buf=new StringBuilder();
                while ((str = reader.readLine()) != null) {
                    buf.append(str+"\n");
                }
                in.close();
                games = buf.toString();
            }
        }

        catch (java.io.FileNotFoundException e) {
        }

        catch (Throwable t) {
            Toast.makeText(this, "Exception: "+t.toString(), Toast.LENGTH_LONG).show();
        }
        return games;
    }

}
