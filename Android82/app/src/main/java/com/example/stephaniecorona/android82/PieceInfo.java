package com.example.stephaniecorona.android82;

/**
 * Created by Stephanie Corona on 4/26/2016.
 */
public class PieceInfo {
    int type;
    int location;
    /**
     * 0- White piece
     * 1- Black piece
     */

    /**
     * 0- pawn
     * 1- rook
     * 2- knight
     * 3- bishop
     * 4- queen
     * 5- king
     *
     */

    public int getType()
    {
        return this.type;
    }

    public int getLocation()
    {
        return this.location;
    }

    public PieceInfo(int type, int location){
        this.type = type;
        this.location = location;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public void setLocation(int location)
    {
        this.location = location;
    }
}
