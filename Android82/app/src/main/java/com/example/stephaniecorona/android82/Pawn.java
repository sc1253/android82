package com.example.stephaniecorona.android82;

/**
 * Created by Stephanie Corona on 4/27/2016.
 */
public class Pawn extends Piece {
    /**
     * Checks if the pawn movement is a legal move
     * return 1 if its a regular one or two up movement
     * return 2 if its a en passant movement
     * returns 3 if it is a pawn promotion
     *
     * @param startLocation
     * @param endLocation
     * @param player
     * @return
     * @author Stephanie Corona
     */
    public static int legalMove(int startLocation, int endLocation, int player) {
        int start[] = new int[2];
        int end[] = new int[2];
        start[0] = startLocation / 8;
        start[1] = startLocation % 8;
        end[0] = endLocation / 8;
        end[1] = endLocation % 8;
        //int Pieces[] = MainActivity.getPieces();

        if (start[1] == end[1]) {
            if (player == 0) {
                //pawn promotion
                if(end[0] == 0){
                    return 3;
                }
                //one move up
                else if (start[0] == end[0] + 1) {
                    return 1;
                }
                //two move up
                else if (start[0] == 6 && end[0] == 4) {
                    return 1;
                }
            }
            else {
                //pawn promotion
                if(end[0] == 7){
                    return 3;
                }
                //one move up
                else if (start[0] == end[0] - 1) {
                    return 1;
                }
                //two moves up
                else if (start[0] == 1 && end[0] == 3) {
                    return 1;
                }
            }
        }
        //En passant
        else if (start[0] == end[0]-1){
            if(start[1] == end[1] -1 || start[1] == end[1] +1 ) {
                return 2;
            }
        }
        else if(start[0] == end[0] +1){
            if(start[1] == end[1] -1 || start[1] == end[1] +1){
                return 2;
            }
        }
        return 0;
    }
}