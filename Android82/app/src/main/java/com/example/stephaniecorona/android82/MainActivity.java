
package com.example.stephaniecorona.android82;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView grid[];
    private boolean pieceSelected = false;
    private boolean undoClicked = false;
    private int startLocation, endLocation, deleteLocal, switchType=-1;
    private String oldColor, newColor;
    private TextView player;
    public static int playersTurn;
    public static boolean draw = false;
    public static int moves[];
    public int numMoves = 0;
    public static int winner=3;
    List<PieceInfo> whitePieces = new ArrayList<>();
    List<PieceInfo> blackPieces = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        player = (TextView) findViewById(R.id.player);
        //ImageView setup
        grid = new ImageView[64];
        grid[0] = (ImageView) findViewById(R.id.a8);
        grid[1] = (ImageView) findViewById(R.id.b8);
        grid[2] = (ImageView) findViewById(R.id.c8);
        grid[3] = (ImageView) findViewById(R.id.d8);
        grid[4] = (ImageView) findViewById(R.id.e8);
        grid[5] = (ImageView) findViewById(R.id.f8);
        grid[6] = (ImageView) findViewById(R.id.g8);
        grid[7] = (ImageView) findViewById(R.id.h8);
        //------------------------------------------------
        grid[8] = (ImageView) findViewById(R.id.a7);
        grid[9] = (ImageView) findViewById(R.id.b7);
        grid[10] = (ImageView) findViewById(R.id.c7);
        grid[11] = (ImageView) findViewById(R.id.d7);
        grid[12] = (ImageView) findViewById(R.id.e7);
        grid[13] = (ImageView) findViewById(R.id.f7);
        grid[14] = (ImageView) findViewById(R.id.g7);
        grid[15] = (ImageView) findViewById(R.id.h7);
        //------------------------------------------------
        grid[16] = (ImageView) findViewById(R.id.a6);
        grid[17] = (ImageView) findViewById(R.id.b6);
        grid[18] = (ImageView) findViewById(R.id.c6);
        grid[19] = (ImageView) findViewById(R.id.d6);
        grid[20] = (ImageView) findViewById(R.id.e6);
        grid[21] = (ImageView) findViewById(R.id.f6);
        grid[22] = (ImageView) findViewById(R.id.g6);
        grid[23] = (ImageView) findViewById(R.id.h6);
        //------------------------------------------------
        grid[24] = (ImageView) findViewById(R.id.a5);
        grid[25] = (ImageView) findViewById(R.id.b5);
        grid[26] = (ImageView) findViewById(R.id.c5);
        grid[27] = (ImageView) findViewById(R.id.d5);
        grid[28] = (ImageView) findViewById(R.id.e5);
        grid[29] = (ImageView) findViewById(R.id.f5);
        grid[30] = (ImageView) findViewById(R.id.g5);
        grid[31] = (ImageView) findViewById(R.id.h5);
        //------------------------------------------------
        grid[32] = (ImageView) findViewById(R.id.a4);
        grid[33] = (ImageView) findViewById(R.id.b4);
        grid[34] = (ImageView) findViewById(R.id.c4);
        grid[35] = (ImageView) findViewById(R.id.d4);
        grid[36] = (ImageView) findViewById(R.id.e4);
        grid[37] = (ImageView) findViewById(R.id.f4);
        grid[38] = (ImageView) findViewById(R.id.g4);
        grid[39] = (ImageView) findViewById(R.id.h4);
        //------------------------------------------------
        grid[40] = (ImageView) findViewById(R.id.a3);
        grid[41] = (ImageView) findViewById(R.id.b3);
        grid[42] = (ImageView) findViewById(R.id.c3);
        grid[43] = (ImageView) findViewById(R.id.d3);
        grid[44] = (ImageView) findViewById(R.id.e3);
        grid[45] = (ImageView) findViewById(R.id.f3);
        grid[46] = (ImageView) findViewById(R.id.g3);
        grid[47] = (ImageView) findViewById(R.id.h3);
        //------------------------------------------------
        grid[48] = (ImageView) findViewById(R.id.a2);
        grid[49] = (ImageView) findViewById(R.id.b2);
        grid[50] = (ImageView) findViewById(R.id.c2);
        grid[51] = (ImageView) findViewById(R.id.d2);
        grid[52] = (ImageView) findViewById(R.id.e2);
        grid[53] = (ImageView) findViewById(R.id.f2);
        grid[54] = (ImageView) findViewById(R.id.g2);
        grid[55] = (ImageView) findViewById(R.id.h2);
        //------------------------------------------------
        grid[56] = (ImageView) findViewById(R.id.a1);
        grid[57] = (ImageView) findViewById(R.id.b1);
        grid[58] = (ImageView) findViewById(R.id.c1);
        grid[59] = (ImageView) findViewById(R.id.d1);
        grid[60] = (ImageView) findViewById(R.id.e1);
        grid[61] = (ImageView) findViewById(R.id.f1);
        grid[62] = (ImageView) findViewById(R.id.g1);
        grid[63] = (ImageView) findViewById(R.id.h1);

        //Assign black piece values to grid
        grid[0].setTag("b_rook");
        grid[1].setTag("b_knight");
        grid[2].setTag("b_bishop");
        grid[3].setTag("b_queen");
        grid[4].setTag("b_king");
        grid[5].setTag("b_bishop");
        grid[6].setTag("b_knight");
        grid[7].setTag("b_rook");

        //Assign black pawn tag to grid values
        for(int i=8; i<16; i++) {
            grid[i].setTag("b_pawn");
        }
        //Assign blank backgrounds to all other grid values
        for(int i=16; i<48; i++){
            grid[i].setTag("blank");
        }
        //Assign white pawn to all other grid values
        for(int i=48; i<56; i++) {
            grid[i].setTag("w_pawn");
        }
        //Assign white pawn tag to grid values
        grid[56].setTag("w_rook");
        grid[57].setTag("w_knight");
        grid[58].setTag("w_bishop");
        grid[59].setTag("w_queen");
        grid[60].setTag("w_king");
        grid[61].setTag("w_bishop");
        grid[62].setTag("w_knight");
        grid[63].setTag("w_rook");

        playersTurn = 0;

        PieceInfo pawn1 = new PieceInfo(0,48);whitePieces.add(pawn1);
        PieceInfo pawn2 = new PieceInfo(0,49);whitePieces.add(pawn2);
        PieceInfo pawn3 = new PieceInfo(0,50);whitePieces.add(pawn3);
        PieceInfo pawn4 = new PieceInfo(0,51);whitePieces.add(pawn4);
        PieceInfo pawn5 = new PieceInfo(0,52);whitePieces.add(pawn5);
        PieceInfo pawn6 = new PieceInfo(0,53);whitePieces.add(pawn6);
        PieceInfo pawn7 = new PieceInfo(0,54);whitePieces.add(pawn7);
        PieceInfo pawn8 = new PieceInfo(0,55);whitePieces.add(pawn8);
        PieceInfo rook1 = new PieceInfo(1,56);whitePieces.add(rook1);
        PieceInfo knight1 = new PieceInfo(2,57);whitePieces.add(knight1);
        PieceInfo bishop1 = new PieceInfo(3,58);whitePieces.add(bishop1);
        PieceInfo queen = new PieceInfo(4,59);whitePieces.add(queen);
        PieceInfo king = new PieceInfo(5,60);whitePieces.add(king);
        PieceInfo bishop2 = new PieceInfo(3,61);whitePieces.add(bishop2);
        PieceInfo knight2 = new PieceInfo(2,62);whitePieces.add(knight2);
        PieceInfo rook2 = new PieceInfo(1,63);whitePieces.add(rook2);

        PieceInfo bpawn1 = new PieceInfo(0,8);blackPieces.add(bpawn1);
        PieceInfo bpawn2 = new PieceInfo(0,9);blackPieces.add(bpawn2);
        PieceInfo bpawn3 = new PieceInfo(0,10);blackPieces.add(bpawn3);
        PieceInfo bpawn4 = new PieceInfo(0,11);blackPieces.add(bpawn4);
        PieceInfo bpawn5 = new PieceInfo(0,12);blackPieces.add(bpawn5);
        PieceInfo bpawn6 = new PieceInfo(0,13);blackPieces.add(bpawn6);
        PieceInfo bpawn7 = new PieceInfo(0,14);blackPieces.add(bpawn7);
        PieceInfo bpawn8 = new PieceInfo(0,15);blackPieces.add(bpawn8);
        PieceInfo brook1 = new PieceInfo(1,0);blackPieces.add(brook1);
        PieceInfo bknight1 = new PieceInfo(2,1);blackPieces.add(bknight1);
        PieceInfo bbishop1 = new PieceInfo(3,2);blackPieces.add(bbishop1);
        PieceInfo bqueen = new PieceInfo(4,3);blackPieces.add(bqueen);
        PieceInfo bking = new PieceInfo(5,4);blackPieces.add(bking);
        PieceInfo bbishop2 = new PieceInfo(3,5);blackPieces.add(bbishop2);
        PieceInfo bknight2 = new PieceInfo(2,6);blackPieces.add(bknight2);
        PieceInfo brook2 = new PieceInfo(1,7);blackPieces.add(brook2);

        moves = new int[1000];
        for(int j=0; j<1000; j++){
            moves[j] = -1;
        }

        startNewGame();
    }

    /**
     * Creates the collapsable menu
     * @param menu
     * @return
     * @author Stephanie Corona
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * When 3 dot menu instance is clicked on it directs you to another page
     * @author Stephanie Corona
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.option1:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.option2:
                Intent intent = new Intent(this, Recorded_Games.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.option3:
                Intent intent2 = new Intent(this, Old_Games.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    /**
     * Every game begins here and the buttons are checked for when they are cicked
     * @author Stephanie Corona
     */
    private void startNewGame(){
        winner=3;
        for (int i=0; i<64; i++){
                grid[i].setOnClickListener(new ButtonClickListener(i));
        }

        Button r = (Button) findViewById(R.id.resign);
        r.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Are you sure?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                draw = true;
                                Intent myIntent = new Intent(MainActivity.this, Winner.class);
                                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(myIntent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                builder.show();
            }
        });

        Button d = (Button) findViewById(R.id.draw);
        d.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("The other Player would like to draw.\nDo you want to call this game a draw?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    draw = true;
                                    Intent myIntent = new Intent(MainActivity.this, Winner.class);
                                    myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(myIntent);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                    builder.show();
                }

        });

        Button u = (Button) findViewById(R.id.undo);
        u.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pieceSelected == false && undoClicked == false) {
                    numMoves -= 2;
                    lastMove(startLocation, endLocation, oldColor, newColor);
                    undoClicked = true;
                }
            }
        });

        Button a = (Button) findViewById(R.id.ai);
        a.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AIMove();
            }
        });
    }

    /**
     * Does the random AIMove()
     * @author Stephanie Corona
     */
    public void AIMove(){
        undoClicked = false;
        Random rand = new Random();
        int  n = rand.nextInt(15);
        boolean found = false;
        if (playersTurn == 0){
            while (!found){
                if(whitePieces.get(n).getLocation() != -1){
                    if(whitePieces.get(n).getType() != 4 || whitePieces.get(n).getType() != 5) {
                        found = true;
                    }
                    else{
                        n = rand.nextInt(15);
                    }
                }
                else{
                    n = rand.nextInt(15);
                }
            }
            int start = whitePieces.get(n).getLocation();
            int end = RandomMove.type(playersTurn,whitePieces.get(n).getType(), whitePieces.get(n).getLocation());
            checkWhitePiece(start, end);
        }
        else{
            while (!found){
                if(blackPieces.get(n).getLocation() != -1){
                    if(blackPieces.get(n).getType() != 4 || blackPieces.get(n).getType() != 5) {
                        found = true;
                    }
                    else{
                        n = rand.nextInt(15);
                    }
                }
                else{
                    n = rand.nextInt(15);
                }
            }
            int start = blackPieces.get(n).getLocation();
            int end = RandomMove.type(playersTurn,blackPieces.get(n).getType(), blackPieces.get(n).getLocation());
            checkBlackPiece(start,end);
        }
    }
    /**
     * Undos the last move
     * @param startLocation
     * @param endLocation
     * @param oldColor
     * @param newColor
     * @author Stephanie Corona
     */
    public void lastMove(int startLocation, int endLocation, String oldColor, String newColor){
        ///grid[startLocation] = grid[endLocation];
        //grid[startLocation].setTag(grid[endLocation].getTag());

        if(oldColor.equals("blank")){
            grid[startLocation].setImageResource(R.drawable.blank);
            grid[startLocation].setTag("blank");
        }
        else if(oldColor.equals("b_pawn")){
            grid[startLocation].setImageResource(R.drawable.b_pawn);
            grid[startLocation].setTag("b_pawn");
        }
        else if(oldColor.equals("b_rook")){
            grid[startLocation].setImageResource(R.drawable.b_rook);
            grid[startLocation].setTag("b_rook");
        }
        else if(oldColor.equals("b_knight")){
            grid[startLocation].setImageResource(R.drawable.b_knight);
            grid[startLocation].setTag("b_knight");
        }
        else if(oldColor.equals("b_queen")){
            grid[startLocation].setImageResource(R.drawable.b_queen);
            grid[startLocation].setTag("b_queen");
        }
        else if(oldColor.equals("b_king")){
            grid[startLocation].setImageResource(R.drawable.b_king);
            grid[startLocation].setTag("b_king");
        }
        else if(oldColor.equals("b_bishop")){
            grid[startLocation].setImageResource(R.drawable.b_bishop);
            grid[startLocation].setTag("b_bishop");
        }

        else if(oldColor.equals("w_pawn")){
            grid[startLocation].setImageResource(R.drawable.w_pawn);
            grid[startLocation].setTag("w_pawn");
        }
        else if(oldColor.equals("w_rook")){
            grid[startLocation].setImageResource(R.drawable.w_rook);
            grid[startLocation].setTag("w_rook");
        }
        else if(oldColor.equals("w_knight")){
            grid[startLocation].setImageResource(R.drawable.w_knight);
            grid[startLocation].setTag("w_knight");
        }
        else if(oldColor.equals("w_queen")) {
            grid[startLocation].setImageResource(R.drawable.w_queen);
            grid[startLocation].setTag("w_queen");
        }
        else if(oldColor.equals("w_king")){
            grid[startLocation].setImageResource(R.drawable.w_king);
            grid[startLocation].setTag("w_king");
        }
        else if(oldColor.equals("w_bishop")){
            grid[startLocation].setImageResource(R.drawable.w_bishop);
            grid[startLocation].setTag("w_bishop");
        }

        if(newColor.equals("blank")){
            grid[endLocation].setImageResource(R.drawable.blank);
            grid[endLocation].setTag("blank");
        }
        else if(newColor.equals("b_pawn")){
            grid[endLocation].setImageResource(R.drawable.b_pawn);
            grid[endLocation].setTag("b_pawn");
        }
        else if(newColor.equals("b_rook")){
            grid[endLocation].setImageResource(R.drawable.b_rook);
            grid[endLocation].setTag("b_rook");
        }
        else if(newColor.equals("b_knight")){
            grid[endLocation].setImageResource(R.drawable.b_knight);
            grid[endLocation].setTag("b_knight");
        }
        else if(newColor.equals("b_queen")){
            grid[endLocation].setImageResource(R.drawable.b_queen);
            grid[endLocation].setTag("b_queen");
        }
        else if(newColor.equals("b_king")){
            grid[endLocation].setImageResource(R.drawable.b_king);
            grid[endLocation].setTag("b_king");
        }
        else if(newColor.equals("b_bishop")){
            grid[endLocation].setImageResource(R.drawable.b_bishop);
            grid[endLocation].setTag("b_bishop");
        }

        else if(newColor.equals("w_pawn")){
            grid[endLocation].setImageResource(R.drawable.w_pawn);
            grid[endLocation].setTag("w_pawn");
        }
        else if(newColor.equals("w_rook")){
            grid[endLocation].setImageResource(R.drawable.w_rook);
            grid[endLocation].setTag("w_rook");
        }
        else if(newColor.equals("w_knight")){
            grid[endLocation].setImageResource(R.drawable.w_knight);
            grid[endLocation].setTag("w_knight");
        }
        else if(newColor.equals("w_queen")) {
            grid[endLocation].setImageResource(R.drawable.w_queen);
            grid[endLocation].setTag("w_queen");
        }
        else if(newColor.equals("w_king")){
            grid[endLocation].setImageResource(R.drawable.w_king);
            grid[endLocation].setTag("w_king");
        }
        else if(newColor.equals("w_bishop")){
            grid[endLocation].setImageResource(R.drawable.w_bishop);
            grid[endLocation].setTag("w_bishop");
        }

        if(playersTurn == 0){
            playersTurn = 1;
            player.setText("Black Player's\nTurn");
        }
        else{
            playersTurn = 0;
            player.setText("White Player's\nTurn");
        }
    }

    /**
     * Checks the color of the piece
     * @param piece
     * @return
     * @author Stephanie Corona
     */
    public int pieceColor(String piece){
        char color = piece.charAt(0);
        if (color == 'w'){
            return 0;
        }
        else{
            return 1;
        }
    }

    /**
     * Updates the locations of the pieces for both the white player
     * and black player
     * @param player
     * @param start
     * @param end
     * @author Stephanie Corona
     */
    public void updatePiece(int player, int start, int end){
        if(player == 0){
            for (int i = 0; i < whitePieces.size(); i++) {
                if(whitePieces.get(i).getLocation() == start){
                    whitePieces.get(i).setLocation(end);
                    if(switchType != -1){
                        whitePieces.get(i).setType(switchType);
                    }
                }
            }

            for (int i = 0; i < blackPieces.size(); i++) {
                if(blackPieces.get(i).getLocation() == end){
                    blackPieces.get(i).setLocation(-1);
                    if(blackPieces.get(i).getType() == 5){
                        winner = 0;
                    }
                }
            }
        }
        else{
            for (int i = 0; i < blackPieces.size(); i++) {
                if(blackPieces.get(i).getLocation() == start){
                    blackPieces.get(i).setLocation(end);
                    if(switchType != -1){
                        blackPieces.get(i).setType(switchType);
                    }
                }
            }

            for (int i = 0; i < whitePieces.size(); i++) {
                if(whitePieces.get(i).getLocation() == end){
                    whitePieces.get(i).setLocation(-1);
                    if(whitePieces.get(i).getType() == 5 ){
                        winner = 1;
                    }
                }
            }
        }

    }

    /**
     * checks when a board piece is selected and checks that the move is correct
     * Updates the board each time a legal move is made
     * @author Stephanie Corona
     */
    private class ButtonClickListener implements View.OnClickListener {
        int location;

        public ButtonClickListener(int location) {
            this.location = location;
        }

        public void onClick(View view){

            if(!pieceSelected){
                if(grid[location].getTag() != "blank") {
                    String piece = (String) grid[location].getTag();
                    int localArray;
                    localArray = pieceColor(piece);
                    if (playersTurn == localArray ) {
                        oldColor = piece;
                        grid[location].setImageResource(R.drawable.blank);
                        startLocation = location;
                        undoClicked = false;
                        pieceSelected = true;
                    }
                }
            }
            else{
                String piece = (String) grid[location].getTag();
                if (grid[startLocation].getTag() == "b_rook"){
                    if (Rook.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("b_rook");
                        grid[location].setImageResource(R.drawable.b_rook);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "b_bishop"){
                    if (Bishop.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("b_bishop");
                        grid[location].setImageResource(R.drawable.b_bishop);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "b_king"){
                    if (King.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("b_king");
                        grid[location].setImageResource(R.drawable.b_king);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "b_pawn"){
                    if(Pawn.legalMove(startLocation, location, playersTurn)== 1){
                        grid[location].setTag("b_pawn");
                        grid[location].setImageResource(R.drawable.b_pawn);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        pieceSelected = false;
                    }
                    //en passant movement
                    else if(Pawn.legalMove(startLocation, location, playersTurn) == 2){
                        updateEnpassant(startLocation,location,1);
                        if (deleteLocal != -1) {
                            grid[location].setTag("b_pawn");
                            grid[location].setImageResource(R.drawable.b_pawn);
                            grid[startLocation].setTag("blank");
                            grid[deleteLocal].setImageResource(R.drawable.blank);
                            grid[deleteLocal].setTag("blank");
                            newColor = piece;
                            endLocation = location;
                            //updatePiece(1, startLocation, endLocation);
                            pieceSelected = false;

                            if(endLocation/8 == 7){
                                CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle("Pick a Promotion");
                                builder.setItems(colors, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which){
                                            case 0:
                                                grid[location].setTag("b_queen");
                                                grid[location].setImageResource(R.drawable.b_queen);
                                                switchType = 4;
                                                break;
                                            case 1:
                                                grid[location].setTag("b_knight");
                                                grid[location].setImageResource(R.drawable.b_knight);
                                                switchType = 2;
                                                break;
                                            case 2:
                                                grid[location].setTag("b_bishop");
                                                grid[location].setImageResource(R.drawable.b_bishop);
                                                switchType = 3;
                                                break;
                                            case 3:
                                                grid[location].setTag("b_rook");
                                                grid[location].setImageResource(R.drawable.b_rook);
                                                switchType = 1;
                                                break;
                                        }
                                    }
                                });
                                builder.show();
                                for (int i = 0; i < blackPieces.size(); i++) {
                                    if(blackPieces.get(i).getLocation() == location){
                                        blackPieces.get(i).setType(switchType);
                                    }
                                }
                                switchType = -1;
                            }
                        }
                    }//pawn promotion movement
                    else if(Pawn.legalMove(startLocation, location, playersTurn) == 3){
                        CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Pick a Promotion");
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        grid[location].setTag("b_queen");
                                        grid[location].setImageResource(R.drawable.b_queen);
                                        switchType = 4;
                                        break;
                                    case 1:
                                        grid[location].setTag("b_knight");
                                        grid[location].setImageResource(R.drawable.b_knight);
                                        switchType = 2;
                                        break;
                                    case 2:
                                        grid[location].setTag("b_bishop");
                                        grid[location].setImageResource(R.drawable.b_bishop);
                                        switchType = 3;
                                        break;
                                    case 3:
                                        grid[location].setTag("b_rook");
                                        grid[location].setImageResource(R.drawable.b_rook);
                                        switchType = 1;
                                        break;
                                }
                            }
                        });
                        builder.show();
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        switchType = -1;
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "b_queen"){
                    if (Queen.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("b_queen");
                        grid[location].setImageResource(R.drawable.b_queen);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "b_knight"){
                    if (Knight.legalMove(startLocation, location, playersTurn) == 1){
                        grid[location].setTag("b_knight");
                        grid[location].setImageResource(R.drawable.b_knight);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(1,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "w_bishop"){
                    if (Bishop.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("w_bishop");
                        grid[location].setImageResource(R.drawable.w_bishop);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "w_king"){
                    if (King.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("w_king");
                        grid[location].setImageResource(R.drawable.w_king);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "w_knight"){
                    if (Knight.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("w_knight");
                        grid[location].setImageResource(R.drawable.w_knight);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "w_pawn"){
                    if(Pawn.legalMove(startLocation, location, playersTurn) == 1){
                        grid[location].setTag("w_pawn");
                        grid[location].setImageResource(R.drawable.w_pawn);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        pieceSelected = false;
                    }
                    //en passant
                    else if(Pawn.legalMove(startLocation, location, playersTurn) == 2) {
                        updateEnpassant(startLocation, location, 0);
                        if (deleteLocal != -1) {
                            grid[location].setTag("w_pawn");
                            grid[location].setImageResource(R.drawable.w_pawn);
                            grid[startLocation].setTag("blank");
                            grid[deleteLocal].setImageResource(R.drawable.blank);
                            grid[deleteLocal].setTag("blank");
                            newColor = piece;
                            endLocation = location;
                            pieceSelected = false;

                            if(endLocation/8 == 0){
                                CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle("Pick a Promotion");
                                builder.setItems(colors, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which){
                                            case 0:
                                                grid[location].setTag("w_queen");
                                                grid[location].setImageResource(R.drawable.w_queen);
                                                switchType = 4;
                                                break;
                                            case 1:
                                                grid[location].setTag("w_knight");
                                                grid[location].setImageResource(R.drawable.w_knight);
                                                switchType = 2;
                                                break;
                                            case 2:
                                                grid[location].setTag("w_bishop");
                                                grid[location].setImageResource(R.drawable.w_bishop);
                                                switchType = 3;
                                                break;
                                            case 3:
                                                grid[location].setTag("w_rook");
                                                grid[location].setImageResource(R.drawable.w_rook);
                                                switchType = 1;
                                                break;
                                        }
                                    }
                                });
                                builder.show();
                                for (int i = 0; i < blackPieces.size(); i++) {
                                    if(whitePieces.get(i).getLocation() == location){
                                        whitePieces.get(i).setType(switchType);
                                    }
                                }
                                switchType = -1;
                            }
                        }
                    }//pawn promotion
                    else if(Pawn.legalMove(startLocation, location, playersTurn) == 3){
                        CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Pick a Promotion");
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        grid[location].setTag("w_queen");
                                        grid[location].setImageResource(R.drawable.w_queen);
                                        switchType = 4;
                                        break;
                                    case 1:
                                        grid[location].setTag("w_knight");
                                        grid[location].setImageResource(R.drawable.w_knight);
                                        switchType = 2;
                                        break;
                                    case 2:
                                        grid[location].setTag("w_bishop");
                                        grid[location].setImageResource(R.drawable.w_bishop);
                                        switchType = 3;
                                        break;
                                    case 3:
                                        grid[location].setTag("w_rook");
                                        grid[location].setImageResource(R.drawable.w_rook);
                                        switchType = 1;
                                        break;
                                }
                            }
                        });
                        builder.show();
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        switchType = -1;
                        pieceSelected = false;

                    }
                }
                else if (grid[startLocation].getTag() == "w_queen"){
                    if (Queen.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("w_queen");
                        grid[location].setImageResource(R.drawable.w_queen);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }
                else if (grid[startLocation].getTag() == "w_rook"){
                    if (Rook.legalMove(startLocation, location, playersTurn) == 1) {
                        grid[location].setTag("w_rook");
                        grid[location].setImageResource(R.drawable.w_rook);
                        grid[startLocation].setTag("blank");
                        newColor = piece;
                        endLocation = location;
                        updatePiece(0,startLocation,endLocation);
                        pieceSelected = false;
                    }
                }

                if(!pieceSelected) {
                    moves[numMoves] = startLocation;
                    moves[numMoves+1] = endLocation;
                    numMoves +=2;
                    if (playersTurn == 0) {
                        playersTurn = 1;
                        player.setText("Black Player's\nTurn");
                    } else {
                        playersTurn = 0;
                        player.setText("White Player's\nTurn");
                    }

                    if(winner!=3){
                        Intent myIntent = new Intent(MainActivity.this, Winner.class);
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(myIntent);
                    }
                }
            }

        }
    }

    /**
     * Does the special check for an Enpassant
     * it must be able to erase the image next to it
     * @param start
     * @param end
     * @param player
     */
    public void updateEnpassant(int start, int end, int player){
        deleteLocal = -1;
        if (player==0){
            for(int i=0; i<blackPieces.size(); i++){
                if (blackPieces.get(i).getLocation() == start-1){
                    deleteLocal = start-1;
                }
                else if(blackPieces.get(i).getLocation() == start+1){
                    deleteLocal = start+1;
                }
            }

            if (deleteLocal !=-1){
                for (int i = 0; i < whitePieces.size(); i++) {
                    if(whitePieces.get(i).getLocation() == start){
                        whitePieces.get(i).setLocation(end);
                        if(switchType != -1){
                            whitePieces.get(i).setType(switchType);
                        }
                    }
                }

                for (int i = 0; i < blackPieces.size(); i++) {
                    if(blackPieces.get(i).getLocation() == end){
                        blackPieces.get(i).setLocation(-1);
                        if(blackPieces.get(i).getType() == 5){
                            winner = 0;
                        }
                    }
                    else if(blackPieces.get(i).getLocation() == deleteLocal){
                        blackPieces.get(i).setLocation(-1);
                        if(blackPieces.get(i).getType() == 5){
                            winner = 0;
                        }
                    }
                }
            }
        }
        else{
            for(int i=0; i<whitePieces.size(); i++){
                if (whitePieces.get(i).getLocation() == start-1){
                    deleteLocal = start-1;
                }
                else if(whitePieces.get(i).getLocation() == start+1){
                    deleteLocal = start+1;
                }
            }
            if (deleteLocal !=-1) {
                for (int i = 0; i < blackPieces.size(); i++) {
                    if(blackPieces.get(i).getLocation() == start){
                        blackPieces.get(i).setLocation(end);
                        if(switchType != -1){
                            blackPieces.get(i).setType(switchType);
                        }
                    }
                }

                for (int i = 0; i < whitePieces.size(); i++) {
                    if(whitePieces.get(i).getLocation() == end){
                        whitePieces.get(i).setLocation(-1);
                        if(whitePieces.get(i).getType() == 5 ){
                            winner = 1;
                        }
                    }
                    else if(whitePieces.get(i).getLocation() == deleteLocal){
                        whitePieces.get(i).setLocation(-1);
                        if(whitePieces.get(i).getType() == 5){
                            winner = 1;
                        }
                    }
                }
            }


        }
    }

    /**
     * Makes a random Black Move and updates the imgage
     * Depending on the move
     * @param start
     * @param end
     * @author Stephanie Corona
     */
    public void checkBlackPiece(int start, int end){
        boolean completed = true;
        startLocation = start;
        final int location = end;
        String piece2 = (String) grid[startLocation].getTag();
        oldColor = piece2;

        String piece = (String) grid[location].getTag();

        if (grid[startLocation].getTag() == "b_rook"){
            if (Rook.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("b_rook");
                grid[location].setImageResource(R.drawable.b_rook);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "b_bishop"){
            if (Bishop.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("b_bishop");
                grid[location].setImageResource(R.drawable.b_bishop);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "b_king"){
            if (King.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("b_king");
                grid[location].setImageResource(R.drawable.b_king);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "b_pawn"){
            if(Pawn.legalMove(startLocation, location, playersTurn)== 1){
                grid[location].setTag("b_pawn");
                grid[location].setImageResource(R.drawable.b_pawn);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                pieceSelected = false;
            }
            //en passant movement
            else if(Pawn.legalMove(startLocation, location, playersTurn) == 2){
                updateEnpassant(startLocation,location,1);
                if (deleteLocal != -1) {
                    grid[location].setTag("b_pawn");
                    grid[location].setImageResource(R.drawable.b_pawn);
                    grid[startLocation].setImageResource(R.drawable.blank);
                    grid[startLocation].setTag("blank");
                    grid[deleteLocal].setImageResource(R.drawable.blank);
                    grid[deleteLocal].setTag("blank");
                    newColor = piece;
                    endLocation = location;
                    //updatePiece(1, startLocation, endLocation);
                    pieceSelected = false;

                    if(endLocation/8 == 7){
                        CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Pick a Promotion");
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        grid[location].setTag("b_queen");
                                        grid[location].setImageResource(R.drawable.b_queen);
                                        switchType = 4;
                                        break;
                                    case 1:
                                        grid[location].setTag("b_knight");
                                        grid[location].setImageResource(R.drawable.b_knight);
                                        switchType = 2;
                                        break;
                                    case 2:
                                        grid[location].setTag("b_bishop");
                                        grid[location].setImageResource(R.drawable.b_bishop);
                                        switchType = 3;
                                        break;
                                    case 3:
                                        grid[location].setTag("b_rook");
                                        grid[location].setImageResource(R.drawable.b_rook);
                                        switchType = 1;
                                        break;
                                }
                            }
                        });
                        builder.show();
                        for (int i = 0; i < blackPieces.size(); i++) {
                            if(blackPieces.get(i).getLocation() == location){
                                blackPieces.get(i).setType(switchType);
                            }
                        }
                        switchType = -1;
                    }
                }
            }//pawn promotion movement
            else if(Pawn.legalMove(startLocation, location, playersTurn) == 3){
                CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Pick a Promotion");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                grid[location].setTag("b_queen");
                                grid[location].setImageResource(R.drawable.b_queen);
                                switchType = 4;
                                break;
                            case 1:
                                grid[location].setTag("b_knight");
                                grid[location].setImageResource(R.drawable.b_knight);
                                switchType = 2;
                                break;
                            case 2:
                                grid[location].setTag("b_bishop");
                                grid[location].setImageResource(R.drawable.b_bishop);
                                switchType = 3;
                                break;
                            case 3:
                                grid[location].setTag("b_rook");
                                grid[location].setImageResource(R.drawable.b_rook);
                                switchType = 1;
                                break;
                        }
                    }
                });
                builder.show();
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                switchType = -1;
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "b_queen"){
            if (Queen.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("b_queen");
                grid[location].setImageResource(R.drawable.b_queen);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "b_knight"){
            if (Knight.legalMove(startLocation, location, playersTurn) == 1){
                grid[location].setTag("b_knight");
                grid[location].setImageResource(R.drawable.b_knight);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(1,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }

        if(completed) {
            moves[numMoves] = startLocation;
            moves[numMoves+1] = endLocation;
            numMoves +=2;

            if (playersTurn == 0) {
                playersTurn = 1;
                player.setText("Black Player's\nTurn");
            } else {
                playersTurn = 0;
                player.setText("White Player's\nTurn");
            }

            if (winner != 3) {
                Intent myIntent = new Intent(MainActivity.this, Winner.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(myIntent);
            }
        }
    }

    /**
     * Makes a random white move and fixes the imgaes on the screen
     * According to the move
     * @param start
     * @param end
     * @author Stephanie Corona
     */
    public void checkWhitePiece(int start, int end){
        boolean completed=true;
        startLocation = start;
        final int location = end;
        String piece2 = (String) grid[startLocation].getTag();
        oldColor = piece2;
        String piece = (String) grid[location].getTag();
        if (grid[startLocation].getTag() == "w_bishop"){
            if (Bishop.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("w_bishop");
                grid[location].setImageResource(R.drawable.w_bishop);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "w_king"){
            if (King.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("w_king");
                grid[location].setImageResource(R.drawable.w_king);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "w_knight"){
            if (Knight.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("w_knight");
                grid[location].setImageResource(R.drawable.w_knight);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "w_pawn"){
            if(Pawn.legalMove(startLocation, location, playersTurn) == 1){
                grid[location].setTag("w_pawn");
                grid[location].setImageResource(R.drawable.w_pawn);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                pieceSelected = false;
            }
            //en passant
            else if(Pawn.legalMove(startLocation, location, playersTurn) == 2) {
                updateEnpassant(startLocation, location, 0);
                if (deleteLocal != -1) {
                    grid[location].setTag("w_pawn");
                    grid[location].setImageResource(R.drawable.w_pawn);
                    grid[startLocation].setImageResource(R.drawable.blank);
                    grid[startLocation].setTag("blank");
                    grid[deleteLocal].setImageResource(R.drawable.blank);
                    grid[deleteLocal].setTag("blank");
                    newColor = piece;
                    endLocation = location;
                    pieceSelected = false;

                    if(endLocation/8 == 0){
                        CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Pick a Promotion");
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        grid[location].setTag("w_queen");
                                        grid[location].setImageResource(R.drawable.w_queen);
                                        switchType = 4;
                                        break;
                                    case 1:
                                        grid[location].setTag("w_knight");
                                        grid[location].setImageResource(R.drawable.w_knight);
                                        switchType = 2;
                                        break;
                                    case 2:
                                        grid[location].setTag("w_bishop");
                                        grid[location].setImageResource(R.drawable.w_bishop);
                                        switchType = 3;
                                        break;
                                    case 3:
                                        grid[location].setTag("w_rook");
                                        grid[location].setImageResource(R.drawable.w_rook);
                                        switchType = 1;
                                        break;
                                }
                            }
                        });
                        builder.show();
                        for (int i = 0; i < blackPieces.size(); i++) {
                            if(whitePieces.get(i).getLocation() == location){
                                whitePieces.get(i).setType(switchType);
                            }
                        }
                        switchType = -1;
                    }
                }
            }//pawn promotion
            else if(Pawn.legalMove(startLocation, location, playersTurn) == 3){
                CharSequence colors[] = new CharSequence[] {"Queen", "Knight", "Bishop", "Rook"};

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Pick a Promotion");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                grid[location].setTag("w_queen");
                                grid[location].setImageResource(R.drawable.w_queen);
                                switchType = 4;
                                break;
                            case 1:
                                grid[location].setTag("w_knight");
                                grid[location].setImageResource(R.drawable.w_knight);
                                switchType = 2;
                                break;
                            case 2:
                                grid[location].setTag("w_bishop");
                                grid[location].setImageResource(R.drawable.w_bishop);
                                switchType = 3;
                                break;
                            case 3:
                                grid[location].setTag("w_rook");
                                grid[location].setImageResource(R.drawable.w_rook);
                                switchType = 1;
                                break;
                        }
                    }
                });
                builder.show();
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                switchType = -1;
                pieceSelected = false;

            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "w_queen"){
            if (Queen.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("w_queen");
                grid[location].setImageResource(R.drawable.w_queen);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        else if (grid[startLocation].getTag() == "w_rook"){
            if (Rook.legalMove(startLocation, location, playersTurn) == 1) {
                grid[location].setTag("w_rook");
                grid[location].setImageResource(R.drawable.w_rook);
                grid[startLocation].setImageResource(R.drawable.blank);
                grid[startLocation].setTag("blank");
                newColor = piece;
                endLocation = location;
                updatePiece(0,startLocation,endLocation);
                pieceSelected = false;
            }
            else{AIMove();completed=false;}
        }
        //Checks that the move was actually legal
        if(completed) {
            moves[numMoves] = startLocation;
            moves[numMoves + 1] = endLocation;
            numMoves += 2;
            if (playersTurn == 0) {
                playersTurn = 1;
                player.setText("Black Player's\nTurn");
            } else {
                playersTurn = 0;
                player.setText("White Player's\nTurn");
            }

            if (winner != 3) {
                Intent myIntent = new Intent(MainActivity.this, Winner.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(myIntent);
            }
        }
    }
}
