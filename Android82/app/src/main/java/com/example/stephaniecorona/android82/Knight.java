package com.example.stephaniecorona.android82;

/**Check Knight for legal Move
 * Created by Stephanie Corona on 4/27/2016.
 */
public class Knight extends Piece {
    public static int legalMove(int startLocation, int endLocation, int player){
        int start[] = new int[2]; int end[]= new int[2];
        start[0] = startLocation/8; start[1] = startLocation%8;
        end[0] = endLocation/8; end[1] = endLocation%8;

        if(Math.abs(start[0] - end[0]) == 1 && Math.abs(start[1] - end[1]) == 2){
            return 1;
        }
        else if (Math.abs(start[1] - end[1]) == 1 && Math.abs(start[0] - end[0]) == 2){
            return 1;
        }

        return 0;
    }
}

